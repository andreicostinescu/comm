//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 31-Mar-21.
//

#ifndef COMM_SOCKET_SOCKETPARTNER_H
#define COMM_SOCKET_SOCKETPARTNER_H

#include <comm/utils/NetworkIncludes.h>
#include <memory>
#include <string>

namespace comm {
    class SocketPartner {
    public:
        static SocketPartner getAnyPartner(bool overwritePartner = true);

        explicit SocketPartner(bool overwritePartner = false, bool initializePartner = false);

        explicit SocketPartner(std::string const &ip, int port, bool overwritePartner);

        explicit SocketPartner(SocketAddress const &partner, bool overwritePartner = false);

        SocketPartner(SocketPartner const &other);

        SocketPartner(SocketPartner &&other) noexcept;

        virtual ~SocketPartner();

        SocketPartner &operator=(SocketPartner const &other);

        SocketPartner &operator=(SocketPartner &&other) noexcept;

        [[nodiscard]] std::shared_ptr<SocketPartner> clone() const;

        void setOverwrite(bool _overwrite);

        void setPartner(SocketAddress const &_partner);

        void setPartner(std::string const &partnerIP, int partnerPort);

        void setAny();

        [[nodiscard]] bool isAny() const;

        [[nodiscard]] bool getOverwrite() const;

        SocketAddress &getPartner();

        SocketAddressLength &getPartnerSize();

        [[nodiscard]] std::string getPartnerString() const;

        [[nodiscard]] std::string getIP() const;

        [[nodiscard]] int getPort() const;

        void cleanup();

    private:
        void initializeEmptyPartner();

        SocketAddress partner;
        SocketAddressLength addressSize;
        bool overwrite;
    };
}

#endif //COMM_SOCKET_SOCKETPARTNER_H
