//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 14.03.2021.
//

#ifndef COMM_COMMUNICATION_TCPSERVER_H
#define COMM_COMMUNICATION_TCPSERVER_H

#include <comm/communication/Communication.h>
#include <comm/socket/Socket.h>
#include <comm/utils/NetworkIncludes.h>

namespace comm {
    // always make sure that the client closes the TCP socket before the server-created socket closes!
    // https://stackoverflow.com/questions/5106674/error-address-already-in-use-while-binding-socket-with-address-but-the-port-num
    class TCPServer {
    public:
        explicit TCPServer(int port, int backlog = 5, bool setReuseAddress = false);

        virtual ~TCPServer();

        void cleanup();

        bool acceptCommunication(Communication &comm);

    private:
        void initServerSocket();

        void listen() const;

        bool setReuseAddress;
        int port, backlog;
        std::shared_ptr<Socket> socket;
        struct timeval socketAcceptTimeout{};
    };
}

#endif //COMM_COMMUNICATION_TCPSERVER_H
