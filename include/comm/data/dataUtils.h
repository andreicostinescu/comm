//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 24.03.2021.
//

#ifndef COMM_DATA_DATAUTILS_H
#define COMM_DATA_DATAUTILS_H

#include <comm/data/MessageType.h>
#include <comm/data/CommunicationData.h>

namespace comm {
    std::shared_ptr<CommunicationData> createCommunicationDataPtr(MessageType const &messageType);
}

#endif //COMM_DATA_DATAUTILS_H
