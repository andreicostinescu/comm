//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 01-Apr-21.
//

#ifndef COMM_DATA_DATACOLLECTION_H
#define COMM_DATA_DATACOLLECTION_H

#include <comm/data/CommunicationData.h>
#include <functional>
#include <map>
#include <string>
#include <vector>

namespace comm {
    class DataCollection {
    public:
        DataCollection();

        explicit DataCollection(std::function<std::shared_ptr<CommunicationData>(MessageType)> dataCreationFunction);

        virtual ~DataCollection();

        void reset();

        void set(MessageType const &messageType, std::shared_ptr<CommunicationData> commData);

        std::shared_ptr<CommunicationData> &get(MessageType const &messageType);

        [[nodiscard]] std::shared_ptr<CommunicationData> const &get(MessageType const &messageType) const;

    protected:
        std::map<std::string, std::shared_ptr<CommunicationData>> data;
        std::vector<std::string> dataKeys;
        std::function<std::shared_ptr<CommunicationData>(MessageType)> dataCreationFunction;
    };
}


#endif //COMM_DATA_DATACOLLECTION_H
