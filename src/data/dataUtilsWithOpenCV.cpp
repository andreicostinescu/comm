//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 24.03.2021.
//

#include <comm/data/dataUtilsWithOpenCV.h>
#include <comm/data/BytesData.h>
#include <comm/data/CoordinateData.h>
#include <comm/data/ImageDataWithOpenCV.h>
#include <comm/data/ImageEncodeDataWithOpenCV.h>
#include <comm/data/StatusData.h>
#include <stdexcept>

using namespace comm;
using namespace std;

std::shared_ptr<CommunicationData> comm::createCommunicationDataPtrWithOpenCV(MessageType const &messageType) {
    switch (messageType) {
        case MessageType::NOTHING: {
            return nullptr;
        }
        case MessageType::COORDINATE: {
            return std::make_shared<CoordinateData>();
        }
        case MessageType::IMAGE: {
            return std::make_shared<ImageDataWithOpenCV>();
        }
        case MessageType::STATUS: {
            return std::make_shared<StatusData>();
        }
        case MessageType::BYTES: {
            return std::make_shared<BytesData>();
        }
        case MessageType::IMAGE_ENCODE: {
            return std::make_shared<ImageEncodeDataWithOpenCV>();
        }
        default : {
            throw runtime_error("Unknown message type: " + to_string(messageType));
        }
    }
}
