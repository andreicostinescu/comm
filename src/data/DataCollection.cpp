//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 01-Apr-21.
//

#include <comm/data/DataCollection.h>
#include <AndreiUtils/utilsMap.hpp>
#include <comm/data/dataUtils.h>
#include <comm/socket/utils.h>
#include <iostream>
#include <stdexcept>

using namespace comm;
using namespace std;

DataCollection::DataCollection() : DataCollection(createCommunicationDataPtr) {}

DataCollection::DataCollection(function<std::shared_ptr<CommunicationData>(MessageType)> dataCreationFunction) :
        data(), dataKeys(), dataCreationFunction(std::move(dataCreationFunction)) {}

DataCollection::~DataCollection() {
    // cout << "In DataCollection destructor: " << endl;
    this->reset();
}

void DataCollection::reset() {
    this->data.clear();
    this->dataKeys.clear();
}

void DataCollection::set(MessageType const &messageType, std::shared_ptr<CommunicationData> commData) {
    string stringMessageType = messageTypeToString(messageType);
    if (!AndreiUtils::mapContains(this->data, stringMessageType)) {
        this->dataKeys.push_back(stringMessageType);
    }
    this->data[stringMessageType] = std::move(commData);
}

std::shared_ptr<CommunicationData> &DataCollection::get(MessageType const &messageType) {
    std::shared_ptr<CommunicationData> *commData;
    string stringMessageType = messageTypeToString(messageType);
    if (!AndreiUtils::mapGetIfContains(this->data, stringMessageType, commData)) {
        for (auto const &dataKey: this->dataKeys) {
            if (dataKey == stringMessageType) {
                (*cerror) << "The data key assertion will fail for key = " << stringMessageType << endl;
            }
            assert(dataKey != stringMessageType);
        }
        commData = &(AndreiUtils::mapEmplace(this->data, stringMessageType,
                                             this->dataCreationFunction(messageType))->second);
        this->dataKeys.push_back(stringMessageType);
    }
    return *commData;
}

std::shared_ptr<CommunicationData> const &DataCollection::get(MessageType const &messageType) const {
    return AndreiUtils::mapGet(this->data, messageTypeToString(messageType));
}
