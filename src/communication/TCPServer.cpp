//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 14.03.2021.
//

#include <comm/communication/TCPServer.h>
#include <cassert>
#include <comm/socket/utils.h>

using namespace comm;

TCPServer::TCPServer(int port, int backlog, bool setReuseAddress) :
        port(port), backlog(backlog), socket(nullptr), setReuseAddress(setReuseAddress) {
    this->socketAcceptTimeout = {.tv_sec = SOCKET_ACCEPT_TIMEOUT_SECONDS, .tv_usec = 0};
    this->initServerSocket();
}

TCPServer::~TCPServer() {
    this->cleanup();
}

void TCPServer::cleanup() {
    this->socket = nullptr;
}

bool TCPServer::acceptCommunication(Communication &comm) {
    static fd_set readSockets;
    FD_ZERO(&readSockets);
    FD_SET(this->socket->getSocket(), &readSockets);

    int selectResult = select((int) this->socket->getSocket() + 1, &readSockets, nullptr, nullptr,
                              &(this->socketAcceptTimeout));
    if (selectResult > 0) {
        // The accept() call actually accepts an incoming connection
        this->socket->accept(comm.getSocket(SocketType::TCP));
        return true;
    } else if (selectResult == SOCKET_ERROR) {
        printLastError();
        this->initServerSocket();
        return false;
    } else {
        // Timeout occurred!
        assert(selectResult == 0);
        return false;
    }
}

void TCPServer::initServerSocket() {
    if (this->socket != nullptr) {
        this->socket->cleanup();
        this->socket->initialize(SocketType::TCP, SocketPartner::getAnyPartner(), this->port);
    } else {
        this->socket = std::make_shared<Socket>(SocketType::TCP, SocketPartner::getAnyPartner(), this->port);
    }
    if (this->setReuseAddress) {
        this->socket->setOption(SO_REUSEADDR, 1);
    }
    this->listen();
}

void TCPServer::listen() const {
    ::listen(this->socket->getSocket(), this->backlog);
}
