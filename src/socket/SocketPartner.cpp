//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 31-Mar-21.
//

#include <comm/socket/SocketPartner.h>
#include <comm/socket/utils.h>
#include <cstring>

using namespace comm;
using namespace std;

SocketPartner SocketPartner::getAnyPartner(bool overwritePartner) {
    return SocketPartner("0.0.0.0", 0, overwritePartner);
}

SocketPartner::SocketPartner(bool overwritePartner, bool initializePartner) :
        partner(), overwrite(overwritePartner), addressSize(sizeof(SocketAddress)) {
    this->initializeEmptyPartner();
}

SocketPartner::SocketPartner(string const &ip, int port, bool overwritePartner) :
        SocketPartner(overwritePartner, true) {
    this->setPartner(ip, port);
}

SocketPartner::SocketPartner(SocketAddress const &partner, bool overwritePartner) : SocketPartner(overwritePartner) {
    this->setPartner(partner);
}

SocketPartner::SocketPartner(SocketPartner const &other) = default;

SocketPartner::SocketPartner(SocketPartner &&other) noexcept:
        partner(other.partner), addressSize(other.addressSize), overwrite(other.overwrite) {
    other.cleanup();
}

SocketPartner::~SocketPartner() {
    this->cleanup();
}

SocketPartner &SocketPartner::operator=(SocketPartner const &other) {
    if (this != &other) {
        this->partner = other.partner;
        this->addressSize = other.addressSize;
        this->overwrite = other.overwrite;
    }
    return *this;
}

SocketPartner &SocketPartner::operator=(SocketPartner &&other) noexcept {
    if (this != &other) {
        this->partner = other.partner;
        this->addressSize = other.addressSize;
        this->overwrite = other.overwrite;
        other.cleanup();
    }
    return *this;
}

std::shared_ptr<SocketPartner> SocketPartner::clone() const {
    auto copy = std::make_shared<SocketPartner>(this->partner, this->overwrite);
    return copy;
}

void SocketPartner::setOverwrite(bool _overwrite) {
    this->overwrite = _overwrite;
}

void SocketPartner::setPartner(SocketAddress const &_partner) {
    this->initializeEmptyPartner();
    this->partner.sin_family = _partner.sin_family;
    this->partner.sin_port = _partner.sin_port;
    this->partner.sin_addr = _partner.sin_addr;
}

void SocketPartner::setPartner(std::string const &partnerIP, int partnerPort) {
    this->initializeEmptyPartner();
    this->partner.sin_family = AF_INET;
    this->partner.sin_port = htons(partnerPort);
    this->partner.sin_addr.s_addr = partnerIP.empty() ? INADDR_ANY : inet_addr(partnerIP.c_str());
}

void SocketPartner::setAny() {
    this->partner.sin_addr.s_addr = INADDR_ANY;
}

bool SocketPartner::isAny() const {
    return this->partner.sin_addr.s_addr == INADDR_ANY;
}

bool SocketPartner::getOverwrite() const {
    return this->overwrite;
}

SocketAddress &SocketPartner::getPartner() {
    return this->partner;
}

SocketAddressLength &SocketPartner::getPartnerSize() {
    return this->addressSize;
}

std::string SocketPartner::getPartnerString() const {
    return this->getIP() + ":" + to_string(this->getPort());
}

std::string SocketPartner::getIP() const {
    return inet_ntoa(this->partner.sin_addr);
}

int SocketPartner::getPort() const {
    return ntohs(this->partner.sin_port);
}

void SocketPartner::cleanup() {
    this->initializeEmptyPartner();
}

void SocketPartner::initializeEmptyPartner() {
    memset(&this->partner, 0, sizeof(SocketAddress));
}
