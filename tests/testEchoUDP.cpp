//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 20.04.21.
//

#include <atomic>
#include <cassert>
#include <comm/communication/Communication.h>
#include <comm/data/ImageDataWithOpenCV.h>
#include <comm/data/StatusData.h>
#include <iostream>
#include <thread>

using namespace comm;
using namespace std;

void createUDPEcho(std::atomic<bool> &quit, SocketType socketType, int port, bool verbose) {
    assert(socketType == SocketType::UDP || socketType == SocketType::UDP_HEADER);
    Communication comm;
    comm.createSocket(socketType, SocketPartner(true, false), port, 2000, 50);

    ImageDataWithOpenCV image;
    StatusData status;
    MessageType messageType;
    while (!quit) {
        if (!comm.recvMessageType(socketType, messageType, false, false, 0, verbose)) {
            cout << "Error when recvMessageType: " << comm.getErrorCode() << ", " << comm.getErrorString() << endl;
            quit = true;
            break;
        } else if (messageType != MessageType::NOTHING) {
            cout << "Received message type " << messageTypeToString(messageType) << endl;
        }
        if (messageType == MessageType::STATUS) {
            cout << "Connection from " << comm.getPartner(socketType).getPartnerString() << endl;
            comm.setOverwritePartner(socketType, false);
            if (!comm.recvData(socketType, &status, false, false, true, -1, verbose)) {
                cout << "Error when recvData (status): " << comm.getErrorCode() << ", " << comm.getErrorString()
                     << endl;
                quit = true;
                break;
            }
            cout << "Received " << status.getDataSize() << " bytes: " << status.getData() << endl;
            comm.setOverwritePartner(socketType, true);
        } else if (messageType == MessageType::IMAGE) {
            cout << "Connection from " << comm.getPartner(socketType).getPartnerString() << endl;
            comm.setOverwritePartner(socketType, false);
            if (!comm.recvData(socketType, &image, false, false, true, -1, verbose)) {
                cout << "Error when recvData (image): " << comm.getErrorCode() << ", " << comm.getErrorString()
                     << endl;
                quit = true;
                break;
            } else if (!image.isImageDeserialized()) {
                cout << "Error when recvData (image): image is not deserialized!" << endl;
            } else {
                imshow("Received Image", image.getImage());
                cv::waitKey(2);
            }
            comm.setOverwritePartner(socketType, true);
        }
    }
}

void echo(std::atomic<bool> &quit, int port) {
    Communication c;
    c.createSocket(SocketType::UDP, SocketPartner::getAnyPartner(), port);

    char *data = new char[65500];
    bool receivedData;
    while (!quit.load()) {
        if (!c.receiveRaw(SocketType::UDP, data, receivedData)) {
            cout << "Error when receiving..." << endl;
        }
        if (receivedData) {
            cout << "UDP ECHO: " << data << endl;
        }
    }
    delete[] data;
    cout << "Finishing UDP RAW ECHO!" << endl;
}

void sender(std::atomic<bool> &quit, int port) {
    Communication c;
    c.createSocket(SocketType::UDP, SocketPartner("127.0.0.1", port, false), 0, 2000);

    string x;
    char data[65500] = {0};
    while (!quit.load()) {
        getline(cin, x);
        memcpy(data, x.c_str(), x.length());
        data[x.length()] = 0;
        if (!c.sendRaw(SocketType::UDP, x.c_str(), (int) x.length() + 1, 0, false)) {
            quit = true;
        }
        if (x == "q") {
            quit = true;
        }
    }
}

void testEchoUDP() {
    SocketType socketType(SocketType::UDP_HEADER);
    int port = 8400;
    std::atomic<bool> quit(false);
    bool verbose = false;

    thread server(createUDPEcho, std::ref(quit), socketType, port, verbose);

    Communication comm;
    comm.createSocket(socketType, SocketPartner("127.0.0.1", port, false), 10001, 2000, 50);

    string x;
    StatusData s;
    while (true) {
        cin >> x;
        if (x == "q") {
            quit = true;
            break;
        }
        s.setData(x);
        comm.transmitData(socketType, &s, false);
    }
    server.join();
}

void testSendRaw() {
    std::atomic<bool> quit(false);
    int port = 8400;
    thread t = thread(echo, std::ref(quit), port);
    sender(quit, port);
    t.join();
}

int main() {
    cout << "Hello World!" << endl;

    // testEchoUDP();
    testSendRaw();

    return 0;
}