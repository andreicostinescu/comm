//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 29-May-21.
//

#include <AndreiUtils/utilsOpenCV.h>
#include <comm/data/DataCollection.h>
#include <comm/data/ImageEncodeDataWithOpenCV.h>
#include <comm/data/dataUtilsWithOpenCV.h>
#include <gtest/gtest.h>
#include <iostream>

using namespace comm;
using namespace std;

TEST(TestImageEncodeData, TestEncodings) {
    cv::Mat lena = cv::imread("../../data/Lena.png");
    DataCollection data(createCommunicationDataPtrWithOpenCV);
    ImageEncodeDataWithOpenCV image(lena, 0, ImageEncodeData::JPEG), *decodedImage;
    decodedImage = std::dynamic_pointer_cast<ImageEncodeDataWithOpenCV>(data.get(MessageType::IMAGE_ENCODE)).get();
    assert(decodedImage != nullptr);
    cout << "Image size: " << lena.rows << "x" << lena.cols << ": " << lena.type()
         << "; raw image bytes = " << image.getImageBytesSize() << endl;

    Buffer b;
    bool serializeDone, deserializeDone;
    for (int encoding = 0; encoding <= 2; encoding++) {
        int state = 0;
        cout << "Expecting message type: " << messageTypeToString(decodedImage->getMessageType()) << endl;
        image.setEncoding(ImageEncodeData::Encoding(encoding));
        while (true) {
            serializeDone = image.serialize(&b, 0, false, true);
            deserializeDone = decodedImage->deserialize(&b, 0, false, true);
            cout << "Serialize state: " << state << ", buffer content size = " << b.getBufferContentSize() << endl;

            ASSERT_TRUE(deserializeDone == serializeDone);
            if (serializeDone) {
                break;
            }
            state++;
        }

        cout << "Encoded with " << ImageEncodeData::convertEncodingToString(image.getEncoding()) << " and decoded with "
             << ImageEncodeData::convertEncodingToString(decodedImage->getEncoding()) << endl;

        ASSERT_TRUE(decodedImage->getID() == image.getID());
        cout << "Different pixels in image per channel " << cv::sum(decodedImage->getImage() != image.getImage()) / 255
             << endl;
        if (ImageEncodeData::Encoding(encoding) != ImageEncodeData::Encoding::JPEG) {
            EXPECT_TRUE(AndreiUtils::matEqual(decodedImage->getImage(), image.getImage()));
        }
    }
}

void readBinaryFile(string const &path, char *&contentBuffer, int &contentSize) {
    char *buffer;
    long long size;
    ifstream file(path, ios::in | ios::binary | ios::ate);
    size = file.tellg();
    file.seekg(0, ios::beg);
    buffer = new char[size];
    file.read(buffer, size);
    file.close();

    cout << "the complete file is in a buffer" << endl;
    contentBuffer = buffer;
    contentSize = (int) size;
}

TEST(TestImageEncodeData, TestBytes) {
    char *contentBuffer;
    int contentSize;
    readBinaryFile("../../data/Lena.png", contentBuffer, contentSize);
    vector<unsigned char> imageEncodedBytes(contentSize);
    for (int i = 0; i < contentSize; i++) {
        imageEncodedBytes[i] = contentBuffer[i];
    }
    DataCollection data(createCommunicationDataPtrWithOpenCV);
    ImageEncodeDataWithOpenCV image, *decodedImage = std::dynamic_pointer_cast<ImageEncodeDataWithOpenCV>(data.get(
            MessageType::IMAGE_ENCODE)).get();
    ImageEncodeData::Encoding encoding = ImageEncodeData::Encoding::PNG;

    image.setImageEncodedBytes(imageEncodedBytes, 330, 330, (int) CV_8UC3, 0, encoding);
    ASSERT_TRUE(image.getImage().rows == 0);
    ASSERT_TRUE(image.getImage().cols == 0);
    ASSERT_TRUE(image.getImage().type() == 0);
    ASSERT_TRUE(image.getImageBytesSize() == 0);
    cout << "Image size: " << image.getImage().rows << "x" << image.getImage().cols << ": " << image.getImage().type()
         << "; image bytes = " << image.getImageBytesSize() << endl;

    Buffer b;
    bool serializeDone, deserializeDone;

    int state = 0;
    cout << "Expecting message type: " << messageTypeToString(decodedImage->getMessageType()) << endl;
    while (true) {
        cout << "Serialize state: " << state << endl;
        serializeDone = image.serialize(&b, 0, false, true);
        deserializeDone = decodedImage->deserialize(&b, 0, false, true);
        cout << "Serialize state: " << state << ", buffer content size = " << b.getBufferContentSize() << endl;

        ASSERT_TRUE(deserializeDone == serializeDone);
        if (serializeDone) {
            break;
        }
        state++;
    }

    cout << "Encoded with " << ImageEncodeData::convertEncodingToString(image.getEncoding()) << " and decoded with "
         << ImageEncodeData::convertEncodingToString(decodedImage->getEncoding()) << endl;

    ASSERT_TRUE(decodedImage->getID() == image.getID());

    EXPECT_TRUE(AndreiUtils::matEqual(decodedImage->getImage(), cv::imread("../../data/Lena.png")));

    delete[] contentBuffer;
}
