//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 16.03.2021.
//

#include <AndreiUtils/json.hpp>
#include <AndreiUtils/utilsThread.h>
#include <cassert>
#include <comm/communication/Communication.h>
#include <comm/data/StatusData.h>
#include <cstring>
#include <gtest/gtest.h>
#include <iostream>
#include <thread>

using namespace comm;
using namespace std;

void server_1(SocketType udpSocketType, int port, bool verbose) {
    string x = "Thread 2";
    Communication p;
    p.createSocket(udpSocketType, SocketPartner::getAnyPartner(), port, 2000, 5000);
    cout << "Created partner communication..." << endl;
    StatusData status;

    while (true) {
        cout << x << ": In while loop, waiting for connection on " << p.getMyself(udpSocketType).getPartnerString()
             << "..." << endl;
        bool recvRes = p.recvData(udpSocketType, &status, false, false, false, 0, verbose);
        ASSERT_TRUE(recvRes || p.getErrorCode() == -1);
        if (p.getErrorCode() == -1) {
            this_thread::sleep_for(chrono::milliseconds(500));
            continue;
        }
        if (status.getData() != nullptr) {
            cout << x << ": Created partner communication with " << p.getPartnerString(udpSocketType) << endl;
            cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;
            ASSERT_TRUE(strcmp(status.getData(), "Ce faci?") == 0);
            status.setData("Bine!");
            bool transmitRes = p.transmitData(udpSocketType, &status, false, false, -1, verbose);
            ASSERT_TRUE(transmitRes);
            break;
        }
        AndreiUtils::sleepMSec(500);
    }

    cout << this_thread::get_id() << ": Ending server normally!" << endl;
}

void client_1(SocketType udpSocketType, int port, bool verbose) {
    this_thread::sleep_for(std::chrono::seconds(2));

    string x = "Thread 1";
    StatusData status;
    Communication p;
    p.createSocket(udpSocketType, SocketPartner("127.0.0.1", port, false), 0, -1, 5000);
    cout << x << ": Created partner communication" << endl;
    status.setData("Ce faci?");
    cout << x << ": Created status data" << endl;
    bool transmitRes = p.transmitData(udpSocketType, &status, false, false, -1, verbose);
    ASSERT_TRUE(transmitRes);
    cout << x << ": Sent status data" << endl;
    bool recvRes = p.recvData(udpSocketType, &status, false, false, false, -1, verbose);
    ASSERT_TRUE(recvRes);
    cout << x << ": Received status data" << endl;
    cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;
    ASSERT_TRUE(strcmp(status.getData(), "Bine!") == 0);

    cout << this_thread::get_id() << ": Ending client_1 normally!" << endl;
}

void server_2(SocketType udpSocketType, int port, bool verbose) {
    string x = "Thread 2";
    Communication p;
    p.createSocket(udpSocketType, SocketPartner::getAnyPartner(), port, 2000, 5000);
    cout << "Created partner communication..." << endl;
    StatusData status;

    while (true) {
        cout << x << ": In while loop, waiting for connection on " << p.getMyself(udpSocketType).getPartnerString()
             << "..." << endl;
        bool recvRes = p.recvData(udpSocketType, &status, false, false, false, 0, verbose);
        assert(recvRes || p.getErrorCode() == -1);
        if (p.getErrorCode() == -1) {
            this_thread::sleep_for(chrono::milliseconds(500));
            continue;
        }
        if (status.getData() != nullptr) {
            cout << x << ": Created partner communication with " << p.getPartnerString(udpSocketType) << endl;
            cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;
            nlohmann::json recvData = nlohmann::json::parse(string(status.getData()));
            assert(recvData["request"] == "telemetry");
            assert(recvData["q"] == nullptr);
            assert(recvData["robot_state"] == 42);
            assert(recvData["O_T_EE"].size() == 16);
            status.setData("Received data!");
            bool transmitRes = p.transmitData(udpSocketType, &status, false, false, -1, verbose);
            assert(transmitRes);
            break;
        }
        this_thread::sleep_for(chrono::milliseconds(500));
    }

    cout << this_thread::get_id() << ": Ending server_2 normally!" << endl;
}

void client_2(SocketType udpSocketType, int port, bool verbose) {
    this_thread::sleep_for(std::chrono::seconds(2));

    string x = "Thread 1";
    StatusData status;
    Communication p;
    p.createSocket(udpSocketType, SocketPartner("127.0.0.1", port, false), 0, -1, 5000);
    cout << x << ": Created partner communication" << endl;

    nlohmann::json data;
    data["request"] = "telemetry";
    data["q"] = nullptr;
    data["robot_state"] = 42;
    data["O_T_EE"] = {1.0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 2, 3, 1};

    status.setData(data.dump());
    cout << x << ": Created status data" << endl;
    bool transmitRes = p.transmitData(udpSocketType, &status, false, false, -1, verbose);
    assert(transmitRes);
    cout << x << ": Sent status data" << endl;
    bool recvRes = p.recvData(udpSocketType, &status, false, false, false, -1, verbose);
    assert(recvRes);
    cout << x << ": Received status data" << endl;
    cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;
    assert(strcmp(status.getData(), "Received data!") == 0);

    cout << this_thread::get_id() << ": Ending client_2 normally!" << endl;
}

TEST(TestUDPCommunication, TestUDP) {
    thread t(server_1, SocketType::UDP, 8400, false);
    client_1(SocketType::UDP, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, TestUDPHeader) {
    thread t = thread(client_1, SocketType::UDP_HEADER, 8400, false);
    server_1(SocketType::UDP_HEADER, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, TestUDPCliendServerSwitch) {
    thread t(server_1, SocketType::UDP, 8400, false);
    client_1(SocketType::UDP, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, TestUDPHeaderClientServerSwitch) {
    thread t = thread(client_1, SocketType::UDP_HEADER, 8400, false);
    server_1(SocketType::UDP_HEADER, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, TestJsonUDP) {
    thread t(server_2, SocketType::UDP, 8400, false);
    client_2(SocketType::UDP, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, TestJsonUDPHeader) {
    thread t = thread(client_1, SocketType::UDP_HEADER, 8400, false);
    server_2(SocketType::UDP_HEADER, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, TestJsonUDPCliendServerSwitch) {
    thread t(server_2, SocketType::UDP, 8400, false);
    client_2(SocketType::UDP, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, TestJsonUDPHeaderClientServerSwitch) {
    thread t = thread(client_2, SocketType::UDP_HEADER, 8400, false);
    server_2(SocketType::UDP_HEADER, 8400, false);
    t.join();
}

TEST(TestUDPCommunication, DoubleAddressBind) {
    Communication p1, p2;
    ASSERT_NO_THROW(p1.createSocket(SocketType::UDP, SocketPartner("127.0.0.1", 6666, false), 6667, 2000, 1));
    ASSERT_THROW(p2.createSocket(SocketType::UDP, SocketPartner("127.0.0.1", 6666, false), 6667, 2000, 1), std::runtime_error);
    try {
        p2.createSocket(SocketType::UDP, SocketPartner("127.0.0.1", 6666, false), 6667, 2000, 1);
        ASSERT_TRUE(false);
    } catch (std::runtime_error &e) {
        ASSERT_TRUE(strcmp(e.what(), "ERROR binding server socket") == 0);
    }
}
