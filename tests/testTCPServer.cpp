//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 15.03.2021.
//

#include <AndreiUtils/utilsThread.h>
#include <comm/communication/TCPServer.h>
#include <comm/data/StatusData.h>
#include <cstring>
#include <gtest/gtest.h>
#include <iostream>
#include <thread>

using namespace comm;
using namespace std;

void server_1(int port) {
    string x = "Thread 2";
    Communication p;
    StatusData status;
    TCPServer t(port);
    cout << x << ": Created tcp server" << endl;

    cout << x << ": listening for connections..." << endl;

    while (true) {
        cout << x << ": In while loop, waiting for connection..." << endl;
        if (t.acceptCommunication(p)) {
            cout << x << ": Created partner communication with " << p.getPartnerString(SocketType::TCP) << endl;
            bool recvRes = p.recvData(SocketType::TCP, &status, false, false, false, -1, true);
            ASSERT_TRUE(recvRes);
            cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;
            ASSERT_TRUE(strcmp(status.getData(), "Ce faci?") == 0);
            status.setData("Bine!");
            bool transmitRes = p.transmitData(SocketType::TCP, &status, false, false, -1, true);
            ASSERT_TRUE(transmitRes);
            recvRes = p.recvData(SocketType::TCP, &status, false, false, false, -1, true);
            ASSERT_TRUE(recvRes);
            ASSERT_TRUE(strcmp(status.getData(), "Quit!") == 0);
            AndreiUtils::sleepMSec(1000);  // wait time for the client to close connection!
            break;
        }
        AndreiUtils::sleepMSec(1);
    }
}

void client_1(int port) {
    this_thread::sleep_for(std::chrono::seconds(3));

    string x = "Thread 1";
    StatusData status;
    Communication p;
    p.createSocket(SocketType::TCP, SocketPartner("127.0.0.1", port, false));
    cout << x << ": Created partner communication" << endl;
    status.setData("Ce faci?");
    cout << x << ": Created status data" << endl;
    bool transmitRes = p.transmitData(SocketType::TCP, &status, false, false, -1, true);
    ASSERT_TRUE(transmitRes);
    cout << x << ": Sent status data" << endl;
    bool recvRes = p.recvData(SocketType::TCP, &status, false, false, false, -1, true);
    ASSERT_TRUE(recvRes);
    cout << x << ": Received status data" << endl;
    cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;
    ASSERT_TRUE(strcmp(status.getData(), "Bine!") == 0);
    status.setData("Quit!");
    transmitRes = p.transmitData(SocketType::TCP, &status, false, false, -1, true);
    ASSERT_TRUE(transmitRes);
}

TEST(TestTCPServer, ConnectSendReceiveEnd) {
    int port = 8400;
    thread t(server_1, port);
    client_1(port);
    t.join();
}

TEST(TestTCPServer, ConnectSendReceiveEndSwitchServerClient) {
    int port = 8400;
    thread t(client_1, port);
    server_1(port);
    t.join();
}