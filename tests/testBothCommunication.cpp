//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 16.03.2021.
//

#include <AndreiUtils/utilsString.h>
#include <AndreiUtils/utilsThread.h>
#include <cassert>
#include <cstring>
#include <comm/communication/TCPServer.h>
#include <comm/data/StatusData.h>
#include <gtest/gtest.h>
#include <iostream>
#include <thread>

using namespace comm;
using namespace std;

void server_1(int port, int udpMessageLimit) {
    string x = "Thread 2";
    Communication p;
    StatusData status;
    TCPServer t(port);
    cout << x << ": Created tcp server" << endl;

    cout << x << ": listening for connections..." << endl;

    while (true) {
        cout << x << ": In while loop, waiting for connection..." << endl;
        if (t.acceptCommunication(p)) {
            cout << x << ": Created partner communication with " << p.getPartnerString(SocketType::TCP) << endl;
            p.setSocketTimeouts(SocketType::TCP, 2000, 50);
            break;
        }
        AndreiUtils::sleepMSec(1);
    }

    cout << x << ": Starting receive tcp status data..." << endl;
    bool recvRes = p.recvData(SocketType::TCP, &status, false, false, false, -1, true);
    ASSERT_TRUE(recvRes);
    cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;

    status.setData("start");
    cout << x << ": Starting start status data..." << endl;
    bool transmitRes = p.transmitData(SocketType::TCP, &status, false, false, -1, true);
    ASSERT_TRUE(transmitRes);

    this_thread::sleep_for(std::chrono::seconds(3));

    p.createSocket(SocketType::UDP, SocketPartner::getAnyPartner(), port, 2000, 50);
    const char *message;
    while (true) {
        recvRes = p.recvData(SocketType::UDP, &status, false, false, false);
        ASSERT_TRUE(recvRes || p.getErrorCode() == -1);
        if (p.getErrorCode() == -1) {
            continue;
        }
        message = status.getData();
        if (message != nullptr) {
            cout << x << ": Received on UDP \"" << message << "\"" << endl;
            auto split = AndreiUtils::splitString(message, " ");
            if (split.size() == 2 && stoi(split[1]) >= udpMessageLimit) {
                break;
            }
        }
    }

    status.setData("stop");
    transmitRes = p.transmitData(SocketType::TCP, &status, false, false);
    ASSERT_TRUE(transmitRes);

    recvRes = p.recvData(SocketType::TCP, &status, false, false);
    ASSERT_TRUE(recvRes);
    ASSERT_TRUE(strcmp(status.getData(), "Quit!") == 0);

    cout << x << ": server finished normally!" << endl;
}

void test_1(int port) {
    this_thread::sleep_for(std::chrono::seconds(2));

    string x = "Thread 1";
    StatusData tcpStatus, udpStatus;
    SocketPartner partner("127.0.0.1", port, false);
    Communication p;

    p.createSocket(SocketType::TCP, partner, -1, 2000, 50);
    cout << x << ": Created TCP partner communication" << endl;
    p.createSocket(SocketType::UDP, partner, -1, 2000, 50);
    cout << x << ": Created TCP & UDP partner communication" << endl;

    tcpStatus.setData("Robot 1");
    cout << x << ": Created tcpStatus data" << endl;
    bool transmitRes = p.transmitData(SocketType::TCP, &tcpStatus, false, false, -1, true);
    ASSERT_TRUE(transmitRes);
    cout << x << ": Sent tcpStatus data" << endl;
    bool recvRes = p.recvData(SocketType::TCP, &tcpStatus, false, false, false, -1, true);
    ASSERT_TRUE(recvRes);
    cout << x << ": Received tcpStatus data \"" << tcpStatus.getData() << "\" from partner" << endl;
    ASSERT_TRUE(strcmp(tcpStatus.getData(), "start") == 0);

    this_thread::sleep_for(std::chrono::seconds(3));

    int id = 0;
    const char *message;
    while (true) {
        udpStatus.setData("Message " + to_string(id++));
        // cout << x << ": set data for udp transmission" << endl;
        transmitRes = p.transmitData(SocketType::UDP, &udpStatus, false, false);
        ASSERT_TRUE(transmitRes);
        cout << x << ": sent udp data: " << udpStatus.getData() << endl;
        recvRes = p.recvData(SocketType::TCP, &tcpStatus, false, false, false, 0);
        ASSERT_TRUE(recvRes || p.getErrorCode() == -1);
        // cout << x << ": after tcp listen/recv" << endl;
        if (p.getErrorCode() == -1) {
            continue;
        }
        message = tcpStatus.getData();
        if (message != nullptr) {
            cout << x << ": received \"" << message << "\" from server" << endl;
            if (strcmp(message, "stop") == 0) {
                break;
            }
        }
    }

    tcpStatus.setData("Quit!");
    p.transmitData(SocketType::TCP, &tcpStatus, false, false);
    p.closeSocket(SocketType::TCP);

    cout << x << ": client_1 finished normally!" << endl;
}

void server_2(int port) {
    string x = "Thread 2";
    Communication p;
    StatusData status;
    TCPServer t(port);
    cout << x << ": Created tcp server" << endl;

    // t.listen();
    cout << x << ": listening for connections..." << endl;

    while (true) {
        cout << x << ": In while loop, waiting for connection..." << endl;
        if (t.acceptCommunication(p)) {
            cout << x << ": Created partner communication with " << p.getPartnerString(SocketType::TCP) << endl;
            p.setSocketTimeouts(SocketType::TCP, 2000, 50);
            break;
        }
        AndreiUtils::sleepMSec(1);
    }

    status.setData("start");
    cout << x << ": Starting start status data..." << endl;
    bool transmitRes = p.transmitData(SocketType::TCP, &status, false, false, -1, true);
    ASSERT_TRUE(transmitRes);

    cout << x << ": Starting receive tcp status data..." << endl;
    bool recvRes = p.recvData(SocketType::TCP, &status, false, false, false, -1, true);
    ASSERT_TRUE(recvRes);
    ASSERT_TRUE(strcmp(status.getData(), "Robot 1") == 0);
    cout << x << ": Received: \"" << status.getData() << "\" from partner" << endl;

    cout << x << ": server finished normally!" << endl;
}

void test_2(int port) {
    this_thread::sleep_for(std::chrono::seconds(2));

    string x = "Thread 1";
    StatusData tcpStatus, udpStatus;
    SocketPartner partner("127.0.0.1", port, false);
    Communication p;

    p.createSocket(SocketType::TCP, partner, -1, 2000, 50);
    cout << x << ": Created TCP partner communication" << endl;

    bool recvRes = p.recvData(SocketType::TCP, &tcpStatus, false, false, false, -1, true);
    ASSERT_TRUE(recvRes);
    ASSERT_TRUE(strcmp(tcpStatus.getData(), "start") == 0);
    cout << x << ": Received tcpStatus data \"" << tcpStatus.getData() << "\" from partner" << endl;

    tcpStatus.setData("Robot 1");
    cout << x << ": Created tcpStatus data" << endl;
    bool transmitRes = p.transmitData(SocketType::TCP, &tcpStatus, false, false, -1, true);
    ASSERT_TRUE(transmitRes);
    cout << x << ": Sent tcpStatus data" << endl;

    cout << x << ": test_2 finished normally!" << endl;
}

TEST(TestBothCommunication, TestStatusDataPerTCP) {
    int port = 8401;
    thread t(server_2, port);
    test_2(port);
    t.join();
}

TEST(TestBothCommunication, TestStatusDataPerTCPClientServerSwitch) {
    int port = 8401;
    thread t(test_2, port);
    server_2(port);
    t.join();
}

TEST(TestBothCommunication, TestStatusDataPerUDP) {
    int port = 8401, udpMessageLimit = 250;
    thread t = thread(server_1, port, udpMessageLimit);
    test_1(port);
    t.join();
}

TEST(TestBothCommunication, TestStatusDataPerUDPClientServerSwitch) {
    int port = 8401, udpMessageLimit = 250;
    thread t = thread(test_1, port);
    server_1(port, udpMessageLimit);
    t.join();
}
