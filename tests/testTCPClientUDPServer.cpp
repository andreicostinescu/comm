//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 18.03.2021.
//

#include <AndreiUtils/utilsString.h>
#include <cassert>
#include <comm/communication/TCPServer.h>
#include <comm/data/StatusData.h>
#include <cstring>
#include <gtest/gtest.h>
#include <iostream>
#include <thread>

using namespace comm;
using namespace std;

void tcpServer(int tcpPort) {
    TCPServer server(tcpPort);
    Communication partner;
    while (true) {
        if (server.acceptCommunication(partner)) {
            cout << "Accepted connection" << endl;
            break;
        }
    }

    StatusData s;
    while (!partner.recvData(SocketType::TCP, &s, false, false, false, 0, false)) {}
    assert(s.getData() != nullptr);
    int udpPort = stoi(s.getData());
    cout << "Received udp port to communicate on: " << udpPort << endl;
    string udpIP = partner.getPartner(SocketType::TCP).getIP();
    assert(udpIP == AndreiUtils::splitString(partner.getPartnerString(SocketType::TCP), ":")[0]);
    SocketPartner p(udpIP, udpPort, false);
    partner.createSocket(SocketType::UDP, p, -1);
    cout << "UDP Client initialized" << endl;

    partner.closeSocket(SocketType::TCP);

    cout << "Client: UDP sendTo: " << partner.getPartnerString(SocketType::UDP) << endl;
    cout << "Client: UDP myself: " << partner.getMyAddressString(SocketType::UDP) << endl;
    s.setData("Hello!");
    bool transmitRes = partner.transmitData(SocketType::UDP, &s, false, false, 0, false);
    ASSERT_TRUE(transmitRes);
    cout << "Client: UDP sendTo: " << partner.getPartnerString(SocketType::UDP) << endl;
    cout << "Client: UDP myself: " << partner.getMyAddressString(SocketType::UDP) << endl;
    s.setData(partner.getMyAddressString(SocketType::UDP));
    transmitRes = partner.transmitData(SocketType::UDP, &s, false, false, 0, false);
    ASSERT_TRUE(transmitRes);
    cout << "Sent my address" << endl;
    cout << "UDP Server finished normally" << endl;
}

void tcpClient(int tcpPort) {
    this_thread::sleep_for(std::chrono::seconds(1));

    Communication p;
    p.createSocket(SocketType::TCP, SocketPartner("127.0.0.1", tcpPort, false));
    p.createSocket(SocketType::UDP, SocketPartner::getAnyPartner(), 0);
    int udpPort = p.getMyself(SocketType::UDP).getPort();
    cout << "Initialized client!" << endl;
    StatusData s;
    s.setData(to_string(udpPort).c_str());
    bool transmitRes = p.transmitData(SocketType::TCP, &s, false, false);
    assert(transmitRes);
    cout << "Sent udp port: " << udpPort << endl;
    p.closeSocket(SocketType::TCP);  // close TCP socket after final send!
    this_thread::sleep_for(std::chrono::seconds(2));

    cout << endl << endl;
    cout << "Server: UDP sendTo: " << p.getPartnerString(SocketType::UDP) << endl;
    cout << "Server: UDP myself: " << p.getMyAddressString(SocketType::UDP) << endl;
    bool recvRes = p.recvData(SocketType::UDP, &s, false, false, false, 0, false);
    ASSERT_TRUE(recvRes);
    ASSERT_TRUE(strcmp(s.getData(), "Hello!") == 0);
    cout << "Got data: " << s.getData() << "; expected: \"Hello!\"" << endl;
    cout << "Server: UDP sendTo: " << p.getPartnerString(SocketType::UDP) << endl;
    cout << "Server: UDP myself: " << p.getMyAddressString(SocketType::UDP) << endl;
    recvRes = p.recvData(SocketType::UDP, &s, false, false, false, 0, false);
    ASSERT_TRUE(recvRes);
    cout << "Got data: " << s.getData() << "; expected: " << p.getPartnerString(SocketType::UDP) << endl;
    // Only compare the ports!
    ASSERT_TRUE(AndreiUtils::splitString(s.getData(), ":")[1] ==
           AndreiUtils::splitString(p.getPartnerString(SocketType::UDP), ":")[1]);
    cout << "Receive start message" << endl;

    cout << "Test finished normally" << endl;
}

TEST(TestTCPClientUDPServer, TestStatusData) {
    int tcpPort = 8000;
    thread t(tcpServer, tcpPort);
    tcpClient(tcpPort);
    t.join();
}

TEST(TestTCPClientUDPServer, TestStatusDataClientServerSwitch) {
    int tcpPort = 8000;
    thread t(tcpClient, tcpPort);
    tcpServer(tcpPort);
    t.join();
}
