//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 09.04.21.
//

#include <comm/socket/utils.h>
#include <csignal>
#include <iostream>

using namespace comm;
using namespace std;

int main() {
    cout << "Hello World!" << endl;

    cout << "Before signal!" << endl;
    signal(SIGKILL, signalHandler);
    cout << "After signal!" << endl;
    sleep(1);
    // raise(SIGKILL);

    return 0;
}