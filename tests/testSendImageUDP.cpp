//
// Created by Andrei Costinescu (andreicostinescu96@gmail.com) on 23.03.2021.
//

#include <AndreiUtils/utilsOpenCV.h>
#include <AndreiUtils/utilsThread.h>
#include <comm/communication/Communication.h>
#include <comm/communication/TCPServer.h>
#include <comm/data/ImageDataWithOpenCV.h>
#include <comm/data/StatusData.h>
#include <comm/socket/utils.h>
#include <gtest/gtest.h>
#include <iostream>
#include <thread>

using namespace comm;
using namespace cv;
using namespace std;

void sender(SocketType udpSocketType, cv::Mat const &dataToSend, int port) {
    this_thread::sleep_for(std::chrono::seconds(1));

    Communication p;
    p.createSocket(SocketType::TCP, SocketPartner("127.0.0.1", port, false), -1, 2000, 500);
    cout << "Initialized sender tcp client!" << endl;
    p.createSocket(udpSocketType, SocketPartner::getAnyPartner(), 0, 2000, 500);
    int udpPort = p.getMyself(udpSocketType).getPort();
    cout << "Initialized sender udp server!" << endl;
    StatusData s;
    s.setData(to_string(udpPort).c_str());
    bool transmitRes = p.transmitData(SocketType::TCP, &s, false, false);
    ASSERT_TRUE(transmitRes);
    bool recvRes = p.recvData(udpSocketType, &s, false, false, false);
    ASSERT_TRUE(recvRes);
    ASSERT_TRUE(strcmp(s.getData(), "hello") == 0);

    ImageDataWithOpenCV i(dataToSend, 1);
    if (!p.transmitData(udpSocketType, &i, false, false, 0, true)) {
        cout << "Error: " << p.getErrorCode() << "; " << p.getErrorString() << "; " << getLastErrorString() << endl;
        EXPECT_TRUE(false);
    } else {
        cout << "Sent image data " << endl;
    }

    cout << "Sender finished normally" << endl;
}

void receiver(SocketType udpSocketType, cv::Mat const &dataToReceive, int port) {
    TCPServer server(port);
    Communication comm;
    while (true) {
        if (server.acceptCommunication(comm)) {
            cout << "Accepted connection" << endl;
            break;
        }
    }
    comm.setSocketTimeouts(SocketType::TCP, 2000, 500);

    StatusData s;
    while (!comm.recvData(SocketType::TCP, &s, false, false, false, 0, false)) {}
    ASSERT_TRUE(s.getData() != nullptr);
    SocketPartner p(comm.getPartner(SocketType::TCP).getIP(), stoi(s.getData()), false);
    cout << "Connecting to udp: " << p.getPartnerString() << endl;
    comm.createSocket(udpSocketType, p, -1, 2000, 500);

    s.setData("hello");
    bool transmitRes = comm.transmitData(udpSocketType, &s, false, false);
    ASSERT_TRUE(transmitRes);

    this_thread::sleep_for(chrono::seconds(2));
    cout << "\n\n";

    ImageDataWithOpenCV i;
    if (!comm.recvData(udpSocketType, &i, false, false, false, 10, true)) {
        cout << "Error: " << comm.getErrorCode() << "; " << comm.getErrorString() << "; " << getLastErrorString()
             << endl;
        ASSERT_TRUE(false);
    } else if (i.isImageDeserialized()) {
        if (!i.getImage().empty()) {
            ASSERT_TRUE(AndreiUtils::matEqual(i.getImage(), dataToReceive));
        } else {
            cout << "Error: received image is empty!" << endl;
            ASSERT_TRUE(false);
        }
    } else {
        cout << "Error: deserializing image..." << endl;
        ASSERT_TRUE(false);
    }

    cout << "Receiver finished normally!" << endl;
}

void streamUDPServer(std::atomic<bool> &quit, cv::Mat const &dataToSend, int port) {
    TCPServer s(port);
    Communication comm, *sendComm;
    comm.createSocket(SocketType::UDP, SocketPartner(true, false), port, 2000, 50);

    ImageDataWithOpenCV image;
    StatusData status;
    MessageType messageType;
    while (!quit) {
        if (!comm.recvMessageType(SocketType::UDP, messageType, false, false)) {
            cout << "Error when recvMessageType: " << comm.getErrorCode() << ", " << comm.getErrorString() << endl;
            quit = true;
            break;
        }
        if (messageType == MessageType::STATUS) {
            cout << "Connection from " << comm.getPartner(SocketType::UDP).getPartnerString() << endl;
            comm.setOverwritePartner(SocketType::UDP, false);
            if (!comm.recvData(SocketType::UDP, &status, false, false, true, -1, true)) {
                cout << "Error when recvData (status): " << comm.getErrorCode() << ", " << comm.getErrorString()
                     << endl;
                quit = true;
                break;
            }
            SocketType socketType;
            if (strcmp(status.getData(), "TCP") == 0) {
                socketType = SocketType::TCP;
            } else if (strcmp(status.getData(), "UDP") == 0) {
                socketType = SocketType::UDP;
            } else {
                cout << "status data is not the expected TCP or UDP but " << status.getData() << endl;
                break;
            }
            if (socketType == SocketType::TCP) {
                sendComm = new Communication;
                while (!s.acceptCommunication(*sendComm)) {}
            } else {
                sendComm = &comm;
            }
            Mat toSendImage;
            dataToSend.copyTo(toSendImage);
            image.setImage(toSendImage);
            image.setID(1);
            while (image.getID() <= 10) {
                AndreiUtils::sleepMSec(800);
                if (!sendComm->transmitData(socketType, &image, false, true)) {
                    cout << "Error when sendData (image): " << comm.getErrorCode() << ", " << comm.getErrorString()
                         << endl;
                    EXPECT_TRUE(false);
                    break;
                }
                cout << "Sent image: " << image.getID() << endl;
                image.setID(image.getID() + 1);
                toSendImage += 10;
                image.setImage(toSendImage);
            }
            bool recvRes = sendComm->recvData(socketType, &status, false, false);
            EXPECT_TRUE(recvRes);
            EXPECT_TRUE(strcmp(status.getData(), "Quit!") == 0);
            cout << "Received data at the end: \"" << status.getData() << "\"" << endl;
            // at the end
            if (socketType == SocketType::TCP) {
                delete sendComm;
                sendComm = nullptr;  // for good practice!
            }
            comm.setOverwritePartner(SocketType::UDP, true);
        }
    }
}

void streamUDPClient(std::atomic<bool> &quit, cv::Mat const &dataToReceive, SocketType socketType, int port) {
    this_thread::sleep_for(chrono::seconds(1));
    Communication comm;
    comm.createSocket(socketType, SocketPartner("127.0.0.1", port, false), 0, 2000, 1000);

    ImageDataWithOpenCV image;
    StatusData status;
    MessageType messageType;
    status.setData(socketTypeToString(socketType));

    if (!comm.transmitData(socketType, &status, false, true, 0, true)) {
        cout << "Error when sendData (status): " << comm.getErrorCode() << ", " << comm.getErrorString() << endl;
        quit = true;
        return;
    }

    namedWindow("Received UDP image");
    int i = 0;
    while (i < 10) {
        cout << "At i = " << i << endl;
        if (!comm.recvMessageType(socketType, messageType, false, false)) {
            cout << "Error when recvMessageType: " << comm.getErrorCode() << ", " << comm.getErrorString() << endl;
            EXPECT_TRUE(false);
            quit = true;
            break;
        }
        if (messageType != MessageType::IMAGE) {
            cout << "Expected image but received " << messageTypeToString(messageType) << endl;
            EXPECT_TRUE(false);
            break;
        }
        if (!comm.recvData(socketType, &image, false, false, true)) {
            cout << "Error when recvData (status): " << comm.getErrorCode() << ", " << comm.getErrorString() << endl;
            EXPECT_TRUE(false);
            quit = true;
            break;
        } else if (!image.isImageDeserialized()) {
            cout << "Image is not completely deserialized..." << endl;
            EXPECT_TRUE(false);
            break;
        } else if (image.getImage().empty()) {
            cout << "Received image is empty!" << endl;
            EXPECT_TRUE(false);
            break;
        } else {
            EXPECT_TRUE(AndreiUtils::matEqual(image.getImage(), dataToReceive + 10 * i));
            imshow("Received image", image.getImage());
            cv::waitKey(10);
        }
        i++;
    }
    status.setData("Quit!");
    bool transmitRes = comm.transmitData(socketType, &status, false);
    EXPECT_TRUE(transmitRes);
    cout << "Finished client!" << endl;
}

TEST(TestSendImage, SendViaUDP) {
    int port = 8400;
    cv::Mat data = cv::imread("../../data/Lena.png");
    thread t(sender, SocketType::UDP_HEADER, data, port);
    receiver(SocketType::UDP_HEADER, data, port);
    t.join();
}

TEST(TestSendImage, SendViaUDPClientServerSwitch) {
    int port = 8400;
    cv::Mat data = cv::imread("../../data/Lena.png");
    thread t(receiver, SocketType::UDP_HEADER, data, port);
    sender(SocketType::UDP_HEADER, data, port);
    t.join();
}

TEST(TestSendImage, ClientServerImageStream_UDP) {
    int port = 8400;
    std::atomic<bool> quit(false);
    Mat lena = cv::imread("../../data/Lena.png");
    thread server(streamUDPServer, std::ref(quit), lena, port);
    thread client(streamUDPClient, std::ref(quit), lena, SocketType::UDP, port);
    client.join();
    server.join();
}

TEST(TestSendImage, ClientServerImageStream_UDP_HEADER) {
    int port = 8400;
    std::atomic<bool> quit(false);
    Mat lena = cv::imread("../../data/Lena.png");
    thread server(streamUDPServer, std::ref(quit), lena, port);
    thread client(streamUDPClient, std::ref(quit), lena, SocketType::UDP_HEADER, port);
    client.join();
    server.join();
}

TEST(TestSendImage, ClientServerImageStream_TCP) {
    int port = 8400;
    std::atomic<bool> quit(false);
    Mat lena = cv::imread("../../data/Lena.png");
    thread server(streamUDPServer, std::ref(quit), lena, port);
    thread client(streamUDPClient, std::ref(quit), lena, SocketType::TCP, port);
    client.join();
    server.join();
}